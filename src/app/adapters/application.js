import Ember from 'ember';
import DS from 'ember-data';
import ENV from 'pa/config/environment';
/**
 *
 *This is application global adapter, which is responsible for url related stuff
 *
 *@class DS.RESTAdapter
 *@constructor
 */
export default DS.RESTAdapter.extend({
	host:ENV.dbHost,
	shouldReloadAll(){return true;},
	pathForType:function(type){
		return Ember.String.dasherize(type);
	}
});
