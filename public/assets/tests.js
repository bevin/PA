define('pa/tests/adapters/application.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - adapters');
  QUnit.test('adapters/application.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/application.js should pass jshint.');
  });
});
define('pa/tests/app.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - .');
  QUnit.test('app.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass jshint.');
  });
});
define('pa/tests/components/longterm-goal.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - components');
  QUnit.test('components/longterm-goal.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/longterm-goal.js should pass jshint.\ncomponents/longterm-goal.js: line 58, col 17, \'allGoals\' is defined but never used.\ncomponents/longterm-goal.js: line 7, col 9, \'$\' is not defined.\ncomponents/longterm-goal.js: line 8, col 24, \'$\' is not defined.\ncomponents/longterm-goal.js: line 18, col 24, \'$\' is not defined.\ncomponents/longterm-goal.js: line 23, col 26, \'$\' is not defined.\ncomponents/longterm-goal.js: line 65, col 32, \'$\' is not defined.\ncomponents/longterm-goal.js: line 70, col 34, \'$\' is not defined.\n\n7 errors');
  });
});
define('pa/tests/components/table-cell.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - components');
  QUnit.test('components/table-cell.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/table-cell.js should pass jshint.\ncomponents/table-cell.js: line 18, col 42, Missing semicolon.\ncomponents/table-cell.js: line 30, col 51, Missing semicolon.\ncomponents/table-cell.js: line 6, col 9, \'$\' is not defined.\ncomponents/table-cell.js: line 9, col 48, \'$\' is not defined.\ncomponents/table-cell.js: line 10, col 9, \'$\' is not defined.\ncomponents/table-cell.js: line 23, col 17, \'$\' is not defined.\n\n6 errors');
  });
});
define('pa/tests/components/table-row.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - components');
  QUnit.test('components/table-row.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/table-row.js should pass jshint.');
  });
});
define('pa/tests/components/tinymce-editor.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - components');
  QUnit.test('components/tinymce-editor.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/tinymce-editor.js should pass jshint.\ncomponents/tinymce-editor.js: line 53, col 43, \'arg\' is defined but never used.\ncomponents/tinymce-editor.js: line 56, col 58, \'e\' is defined but never used.\ncomponents/tinymce-editor.js: line 56, col 54, \'cm\' is defined but never used.\ncomponents/tinymce-editor.js: line 56, col 49, \'edd\' is defined but never used.\ncomponents/tinymce-editor.js: line 58, col 22, Expected an assignment or function call and instead saw an expression.\ncomponents/tinymce-editor.js: line 60, col 43, \'e\' is defined but never used.\ncomponents/tinymce-editor.js: line 150, col 33, Expected \'===\' and instead saw \'==\'.\ncomponents/tinymce-editor.js: line 156, col 33, Missing semicolon.\ncomponents/tinymce-editor.js: line 157, col 40, Missing semicolon.\ncomponents/tinymce-editor.js: line 116, col 22, \'tinymce\' is not defined.\ncomponents/tinymce-editor.js: line 131, col 25, \'tinymce\' is not defined.\ncomponents/tinymce-editor.js: line 143, col 9, \'tinymce\' is not defined.\ncomponents/tinymce-editor.js: line 144, col 9, \'tinymce\' is not defined.\ncomponents/tinymce-editor.js: line 149, col 26, \'tinymce\' is not defined.\ncomponents/tinymce-editor.js: line 130, col 23, \'$\' is not defined.\ncomponents/tinymce-editor.js: line 134, col 13, \'$\' is not defined.\ncomponents/tinymce-editor.js: line 139, col 13, \'$\' is not defined.\n\n17 errors');
  });
});
define('pa/tests/components/weekly-calendar.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - components');
  QUnit.test('components/weekly-calendar.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/weekly-calendar.js should pass jshint.');
  });
});
define('pa/tests/components/weekly-goal.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - components');
  QUnit.test('components/weekly-goal.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/weekly-goal.js should pass jshint.\ncomponents/weekly-goal.js: line 9, col 50, Missing semicolon.\ncomponents/weekly-goal.js: line 48, col 15, Missing semicolon.\ncomponents/weekly-goal.js: line 63, col 44, Missing semicolon.\ncomponents/weekly-goal.js: line 43, col 17, \'allGoals\' is defined but never used.\ncomponents/weekly-goal.js: line 80, col 62, Missing semicolon.\ncomponents/weekly-goal.js: line 85, col 52, Missing semicolon.\ncomponents/weekly-goal.js: line 89, col 11, Missing semicolon.\ncomponents/weekly-goal.js: line 100, col 37, Expected \'===\' and instead saw \'==\'.\ncomponents/weekly-goal.js: line 106, col 31, Expected \'===\' and instead saw \'==\'.\ncomponents/weekly-goal.js: line 14, col 48, \'$\' is not defined.\ncomponents/weekly-goal.js: line 18, col 9, \'$\' is not defined.\ncomponents/weekly-goal.js: line 19, col 9, \'$\' is not defined.\ncomponents/weekly-goal.js: line 20, col 9, \'$\' is not defined.\ncomponents/weekly-goal.js: line 23, col 24, \'$\' is not defined.\ncomponents/weekly-goal.js: line 28, col 26, \'$\' is not defined.\ncomponents/weekly-goal.js: line 51, col 32, \'$\' is not defined.\ncomponents/weekly-goal.js: line 56, col 34, \'$\' is not defined.\ncomponents/weekly-goal.js: line 76, col 28, \'$\' is not defined.\ncomponents/weekly-goal.js: line 82, col 26, \'$\' is not defined.\n\n19 errors');
  });
});
define('pa/tests/controllers/mission-statement.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - controllers');
  QUnit.test('controllers/mission-statement.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/mission-statement.js should pass jshint.');
  });
});
define('pa/tests/helpers/destroy-app', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = destroyApp;

  function destroyApp(application) {
    _ember['default'].run(application, 'destroy');
  }
});
define('pa/tests/helpers/destroy-app.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - helpers');
  QUnit.test('helpers/destroy-app.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass jshint.');
  });
});
define('pa/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'pa/tests/helpers/start-app', 'pa/tests/helpers/destroy-app'], function (exports, _qunit, _paTestsHelpersStartApp, _paTestsHelpersDestroyApp) {
  exports['default'] = function (name) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _paTestsHelpersStartApp['default'])();

        if (options.beforeEach) {
          options.beforeEach.apply(this, arguments);
        }
      },

      afterEach: function afterEach() {
        (0, _paTestsHelpersDestroyApp['default'])(this.application);

        if (options.afterEach) {
          options.afterEach.apply(this, arguments);
        }
      }
    });
  };
});
define('pa/tests/helpers/module-for-acceptance.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - helpers');
  QUnit.test('helpers/module-for-acceptance.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass jshint.');
  });
});
define('pa/tests/helpers/resolver', ['exports', 'ember/resolver', 'pa/config/environment'], function (exports, _emberResolver, _paConfigEnvironment) {

  var resolver = _emberResolver['default'].create();

  resolver.namespace = {
    modulePrefix: _paConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _paConfigEnvironment['default'].podModulePrefix
  };

  exports['default'] = resolver;
});
define('pa/tests/helpers/resolver.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - helpers');
  QUnit.test('helpers/resolver.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/resolver.js should pass jshint.');
  });
});
define('pa/tests/helpers/start-app', ['exports', 'ember', 'pa/app', 'pa/config/environment'], function (exports, _ember, _paApp, _paConfigEnvironment) {
  exports['default'] = startApp;

  function startApp(attrs) {
    var application = undefined;

    var attributes = _ember['default'].merge({}, _paConfigEnvironment['default'].APP);
    attributes = _ember['default'].merge(attributes, attrs); // use defaults, but you can override;

    _ember['default'].run(function () {
      application = _paApp['default'].create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
    });

    return application;
  }
});
define('pa/tests/helpers/start-app.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - helpers');
  QUnit.test('helpers/start-app.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass jshint.');
  });
});
define('pa/tests/models/longterm-goal.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - models');
  QUnit.test('models/longterm-goal.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/longterm-goal.js should pass jshint.');
  });
});
define('pa/tests/models/mission-statement.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - models');
  QUnit.test('models/mission-statement.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/mission-statement.js should pass jshint.');
  });
});
define('pa/tests/models/role.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - models');
  QUnit.test('models/role.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/role.js should pass jshint.');
  });
});
define('pa/tests/models/weekly-goal.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - models');
  QUnit.test('models/weekly-goal.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/weekly-goal.js should pass jshint.');
  });
});
define('pa/tests/router.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - .');
  QUnit.test('router.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass jshint.');
  });
});
define('pa/tests/routes/index.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - routes');
  QUnit.test('routes/index.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/index.js should pass jshint.');
  });
});
define('pa/tests/routes/longterm-goals.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - routes');
  QUnit.test('routes/longterm-goals.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'routes/longterm-goals.js should pass jshint.\nroutes/longterm-goals.js: line 12, col 50, Missing semicolon.\nroutes/longterm-goals.js: line 13, col 50, Missing semicolon.\n\n2 errors');
  });
});
define('pa/tests/routes/mission-statement.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - routes');
  QUnit.test('routes/mission-statement.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/mission-statement.js should pass jshint.');
  });
});
define('pa/tests/routes/weekly-goals.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - routes');
  QUnit.test('routes/weekly-goals.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(false, 'routes/weekly-goals.js should pass jshint.\nroutes/weekly-goals.js: line 12, col 50, Missing semicolon.\nroutes/weekly-goals.js: line 13, col 50, Missing semicolon.\n\n2 errors');
  });
});
define('pa/tests/serializers/application.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - serializers');
  QUnit.test('serializers/application.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/application.js should pass jshint.');
  });
});
define('pa/tests/test-helper', ['exports', 'pa/tests/helpers/resolver', 'ember-qunit'], function (exports, _paTestsHelpersResolver, _emberQunit) {

  (0, _emberQunit.setResolver)(_paTestsHelpersResolver['default']);
});
define('pa/tests/test-helper.jshint', ['exports'], function (exports) {
  'use strict';

  QUnit.module('JSHint - .');
  QUnit.test('test-helper.js should pass jshint', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass jshint.');
  });
});
/* jshint ignore:start */

require('pa/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;

/* jshint ignore:end */
//# sourceMappingURL=tests.map