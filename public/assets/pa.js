"use strict";
/* jshint ignore:start */

/* jshint ignore:end */

define('pa/adapters/application', ['exports', 'ember', 'ember-data', 'pa/config/environment'], function (exports, _ember, _emberData, _paConfigEnvironment) {
  /**
   *
   *This is application global adapter, which is responsible for url related stuff
   *
   *@class DS.RESTAdapter
   *@constructor
   */
  exports['default'] = _emberData['default'].RESTAdapter.extend({
    host: _paConfigEnvironment['default'].dbHost,
    shouldReloadAll: function shouldReloadAll() {
      return true;
    },
    pathForType: function pathForType(type) {
      return _ember['default'].String.dasherize(type);
    }
  });
});
define('pa/app', ['exports', 'ember', 'ember/resolver', 'ember/load-initializers', 'pa/config/environment'], function (exports, _ember, _emberResolver, _emberLoadInitializers, _paConfigEnvironment) {

  var App = undefined;

  _ember['default'].MODEL_FACTORY_INJECTIONS = true;
  _ember['default'].run.backburner.DEBUG = true;
  _ember['default'].LOG_STACKTRACE_ON_DEPRECATION = true;
  _ember['default'].LOG_BINDINGS = true;

  App = _ember['default'].Application.extend({
    modulePrefix: _paConfigEnvironment['default'].modulePrefix,
    podModulePrefix: _paConfigEnvironment['default'].podModulePrefix,
    Resolver: _emberResolver['default']
  });

  (0, _emberLoadInitializers['default'])(App, _paConfigEnvironment['default'].modulePrefix);

  exports['default'] = App;
});
define('pa/components/app-version', ['exports', 'ember-cli-app-version/components/app-version', 'pa/config/environment'], function (exports, _emberCliAppVersionComponentsAppVersion, _paConfigEnvironment) {

  var name = _paConfigEnvironment['default'].APP.name;
  var version = _paConfigEnvironment['default'].APP.version;

  exports['default'] = _emberCliAppVersionComponentsAppVersion['default'].extend({
    version: version,
    name: name
  });
});
define('pa/components/longterm-goal', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Component.extend({
		classNames: 'role-goal',
		didInsertElement: function didInsertElement() {
			var self = this;
			$.fn.editable.defaults.mode = 'inline';
			var roleEdit = $(this.element.getElementsByClassName('role-editable'));
			roleEdit.editable({
				type: 'text',
				display: function display() {
					return false;
				},
				success: function success(response, newValue) {
					var role = self.get('role');
					role.set('name', newValue);
					role.save();
				}
			});
			var goalEdit = $(this.element.getElementsByClassName('goal-editable'));
			goalEdit.editable({
				type: 'text',
				display: function display() {
					return false;
				},
				success: function success(response, newValue) {
					var id = $(this).data('id');
					var model = self.get('goals').findBy('id', id);
					model.set('content', newValue);
					model.save();
				}
			});
		},
		goals: (function () {
			return this.get('all-goals').filterBy('roleId', this.get('role').id);
		}).property('all-goals.[]'),
		actions: {
			deleteRole: function deleteRole() {
				var store = this.get('targetObject.store');

				//destroy longterm goals within to this role
				var goals = this.get('goals');
				while (goals.length) {
					goals.pop().destroyRecord();
				}

				//destroy weekly records belong to this role
				store.query('weekly-goal', { roleId: this.get('role.id') }).then(function (models) {
					models.forEach(function (item) {
						item.destroyRecord();
					});
				});

				//destroy the role
				this.get('role').destroyRecord();
			},
			newGoal: function newGoal() {
				var store = this.get('targetObject.store');
				var allGoals = this.get('all-goals');
				var goal = store.createRecord('longterm-goal', {
					roleId: this.get('role.id'),
					content: 'New goal'
				});
				var self = this;
				goal.save().then(function () {
					var goalEdit = $(self.element.getElementsByClassName('goal-editable'));
					goalEdit.editable({
						type: 'text',
						display: function display() {
							return false;
						},
						success: function success(response, newValue) {
							var id = $(this).data('id');
							var model = self.get('goals').findBy('id', id);
							model.set('content', newValue);
							model.save();
						}
					});
				});
			},
			deleteGoal: function deleteGoal(id) {
				var model = this.get('goals').findBy('id', id);
				model.destroyRecord();
			}

		}
	});
});
define('pa/components/table-cell', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Component.extend({
		classNames: 'table-cell',
		didInsertElement: function didInsertElement() {
			$(this.element.getElementsByClassName('event')).tooltip();
		},
		dragStart: function dragStart(event) {
			event.dataTransfer.setData('text/data', $(event.target).data('id'));
			$(event.target).tooltip('hide');
		},
		dragOver: function dragOver(event) {
			event.preventDefault();
		},
		drop: function drop(event) {
			var id = event.dataTransfer.getData('text/data');
			var goals = this.get('goals');
			var model = goals.findBy('id', id);
			model.set('calendarLoc', this.get('id'));
			var self = this;
			model.save().then(function () {
				$(self.element.getElementsByClassName('event')).tooltip();
			});
		},
		myGoals: _ember['default'].computed('goals.@each.calendarLoc', function () {
			return this.get('goals').filterBy('calendarLoc', this.get('id'));
		}),
		id: _ember['default'].computed('rowId', 'colId', function () {
			return this.get('rowId') + this.get('colId');
		})

	});
});
define('pa/components/table-row', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Component.extend({
		tagName: 'tr',
		idList: ['0', '1', '2', '3', '4', '5', '6']
	});
});
define('pa/components/tinymce-editor', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Component.extend({
		editor: null,
		_suspendValueChange: false,
		isElementInserted: false,
		classNames: 'editer-view',

		didInsertElement: function didInsertElement() {
			var self = this;

			this.set('isElementInserted', true);

			if (window.isLoadingRte) {
				var interval = setInterval(function () {
					if (window.isLoadingRte === false) {
						clearInterval(interval);
						self.initTinyMce();
					}
				}, 200);

				return;
			}

			if (window.tinymce) {
				this.initTinyMce();
			} else {
				window.isLoadingRte = true;
				window.isLoadingRte = false;
				self.initTinyMce();
			}
		},

		initTinyMce: function initTinyMce() {
			var self = this;

			if (this.get('isElementInserted') !== true) {
				return;
			}

			if (window.tinymce) {
				window.tinymce.suffix = '.min';
				window.tinymce.init({
					selector: "#statement",
					setup: function setup(ed) {
						self.set("editor", ed);
						ed.on("keyup change", function () {
							self.suspendValueChange(function () {
								self.set("value", ed.getContent());
							});
						});
						ed.on('init', function (arg) {
							ed.setContent(self.get('value'));
						}), ed.on('NodeChange', function (edd, cm, e) {
							self.fixToolbarLocation();
						});

						ed.on('blur', function (e) {
							self.finishEditing();
						});
					},
					inline: true,
					format: 'raw',
					trusted: true,
					plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools'],
					toolbar1: 'insertfile undo redo | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
					toolbar2: 'styleselect | bold italic | forecolor backcolor emoticons|fontselect | fontsizeselect | print media ',
					skin: 'lightgray',
					theme: 'modern',
					fixed_toolbar_container: '#toolbar',
					content_css: 'tinymce/content.css',
					readonly: 1,
					toolbar_items_size: 'small'
				});
			}
		},
		suspendValueChange: function suspendValueChange(cb) {
			this._suspendValueChange = true;
			cb();
			this._suspendValueChange = false;
		},
		valueChanged: (function () {
			if (this._suspendValueChange) {
				return;
			}
			var content = this.get("value");

			if (_ember['default'].isPresent(content)) {
				this.get("editor").setContent(content);
			}
		}).observes("value"),

		willClearRender: function willClearRender() {
			this.set('isElementInserted', false);
			var self = this;
			setTimeout(function () {
				var editor = self.get("editor");
				if (editor == null) {
					setTimeout(function () {
						self.get('editor').remove();
					}, 250);
				}
				editor.remove();
			}, 250);
		},
		fixToolbarLocation: function fixToolbarLocation() {
			var selectionTop = 0;
			var editor = tinymce.activeEditor;
			var rects = editor.selection.getRng().getClientRects();
			if (rects.length > 0) {
				for (var i = 0; i < rects.length; i++) {
					selectionTop += rects[i].top;
				}
				selectionTop = selectionTop / rects.length;
			} else {
				selectionTop = editor.selection.getNode().getBoundingClientRect().top;
			}
			var toolbar = $(document.querySelector('#toolbar'));
			if (selectionTop < tinymce.activeEditor.bodyElement.offsetHeight / 2) {
				toolbar.css('top', 'calc(100% - ' + toolbar.height() + 'px )');
				$('body').addClass('toolbar-bottom');
			} else {
				toolbar.css('top', '0px');
				$('body').removeClass('toolbar-bottom');
			}
		},
		finishEditing: function finishEditing() {
			tinymce.activeEditor.readonly = 1;
			tinymce.activeEditor.getBody().setAttribute('contenteditable', false);
			this.get('onBlur')();
		},
		actions: {
			editStatement: function editStatement() {
				var editor = tinymce.activeEditor;
				if (editor.readonly == 0) {
					return;
				}
				editor.getBody().setAttribute('contenteditable', true);
				editor.readonly = 0;
				editor.fire('focus');
				editor.selection.collapse();
			}
		}
	});
});
define('pa/components/weekly-calendar', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Component.extend({
		classNames: 'weekly-calendar',
		times: ['08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00']
	});
});
define('pa/components/weekly-goal', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Component.extend({
		classNames: 'weekly-goal',
		priorityList: (function () {
			var source = [];
			for (var i = 0; i < this.get('all-goals.length'); i++) {
				source.push({ value: i, text: String(i) });
			}
			return source;
		}).property('all-goals.length'),
		dragStart: function dragStart(event) {
			event.dataTransfer.setData('text/data', $(event.target).data('id'));
		},
		didInsertElement: function didInsertElement() {
			var self = this;
			$.fn.editable.defaults.mode = 'inline';
			$.fn.editable.defaults.toggle = 'dblclick';
			$.fn.editable.defaults.autotext = 'always';

			//xeditable goal
			var goalEdit = $(this.element.getElementsByClassName('goal-editable'));
			goalEdit.editable({
				type: 'text',
				display: function display() {
					return false;
				},
				success: function success(response, newValue) {
					var id = $(this).data('id');
					var model = self.get('goals').findBy('id', id);
					model.set('content', newValue);
					model.save();
				}
			});
			//xeditable priority
			this.calcPriorities();
		},
		goals: (function () {
			return this.get('all-goals').filterBy('roleId', this.get('role').id);
		}).property('all-goals.[]'),
		actions: {
			newGoal: function newGoal() {
				var store = this.get('targetObject.store');
				var allGoals = this.get('all-goals');
				var goal = store.createRecord('weekly-goal', {
					roleId: this.get('role.id'),
					content: 'New goal',
					priority: 0
				});
				var self = this;
				goal.save().then(function () {
					var goalEdit = $(self.element.getElementsByClassName('goal-editable'));
					goalEdit.editable({
						type: 'text',
						display: function display() {
							return false;
						},
						success: function success(response, newValue) {
							var id = $(this).data('id');
							var model = self.get('goals').findBy('id', id);
							model.set('content', newValue);
							model.save();
						}
					});
					//xeditable priority
					self.sortPriorities(goal, 0);
					self.calcPriorities();
				});
			},
			deleteGoal: function deleteGoal(id) {
				var model = this.get('goals').findBy('id', id);
				this.sortPriorities(model, -1);
				model.destroyRecord();
			}

		},
		calcPriorities: function calcPriorities() {
			var self = this;
			var priorityEdit = $(this.element.getElementsByClassName('priority-editable'));
			priorityEdit.editable({
				type: 'select',
				display: function display() {
					return false;
				},
				source: function source() {
					return self.get('priorityList');
				},
				success: function success(response, newValue) {
					var id = $(this).data('id');
					var model = self.get('goals').findBy('id', id);
					//sort anyother model priorities, skip input model for newValue
					self.sortPriorities(model, newValue);
					model.set('priority', newValue);
					model.save();
				}
			});
		},
		sortPriorities: function sortPriorities(currentModel, newPriority) {
			var goals = this.get('all-goals').sortBy('priority');

			//give new priority to every model, start from 0
			var lastPriority = 0;
			for (var i = 0; i < goals.length; i++) {
				// i is proirity
				if (goals[i].get('id') == currentModel.get('id')) {
					//skip currentModel
					continue;
				}
				//skip currentModel priority too
				if (lastPriority == newPriority) {
					lastPriority++;
				}
				goals[i].set('priority', lastPriority);
				goals[i].save();
				lastPriority++;
			}
		}
	});
});
define('pa/controllers/array', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller;
});
define('pa/controllers/mission-statement', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Controller.extend({
		actions: {
			tinymceChanged: function tinymceChanged() {
				this.model.save();
			}
		}
	});
});
define('pa/controllers/object', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Controller;
});
define('pa/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'pa/config/environment'], function (exports, _emberCliAppVersionInitializerFactory, _paConfigEnvironment) {
  exports['default'] = {
    name: 'App Version',
    initialize: (0, _emberCliAppVersionInitializerFactory['default'])(_paConfigEnvironment['default'].APP.name, _paConfigEnvironment['default'].APP.version)
  };
});
define('pa/initializers/export-application-global', ['exports', 'ember', 'pa/config/environment'], function (exports, _ember, _paConfigEnvironment) {
  exports.initialize = initialize;

  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_paConfigEnvironment['default'].exportApplicationGlobal !== false) {
      var value = _paConfigEnvironment['default'].exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = _ember['default'].String.classify(_paConfigEnvironment['default'].modulePrefix);
      }

      if (!window[globalName]) {
        window[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete window[globalName];
          }
        });
      }
    }
  }

  exports['default'] = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('pa/models/longterm-goal', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    content: _emberData['default'].attr('string'),
    roleId: _emberData['default'].attr('string')
  });
});
define('pa/models/mission-statement', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    statement: _emberData['default'].attr('string')
  });
});
define('pa/models/role', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    name: _emberData['default'].attr('string')
  });
});
define('pa/models/weekly-goal', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].Model.extend({
    roleId: _emberData['default'].attr('string'),
    priority: _emberData['default'].attr('number'),
    content: _emberData['default'].attr('string'),
    calendarLoc: _emberData['default'].attr('string')
  });
});
define('pa/router', ['exports', 'ember', 'pa/config/environment'], function (exports, _ember, _paConfigEnvironment) {

	var Router = _ember['default'].Router.extend({
		location: _paConfigEnvironment['default'].locationType
	});

	Router.map(function () {
		this.route('mission-statement', { path: '/' });
		this.route('longterm-goals');
		this.route('weekly-goals');
	});

	exports['default'] = Router;
});
define('pa/routes/index', ['exports', 'ember'], function (exports, _ember) {
  exports['default'] = _ember['default'].Route.extend({});
});
define('pa/routes/longterm-goals', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({
		model: function model() {
			return _ember['default'].RSVP.hash({
				roles: this.store.findAll('role'),
				goals: this.store.findAll('longterm-goal')
			});
		},
		setupController: function setupController(controller, model) {
			this._super.apply(this, arguments);
			_ember['default'].set(controller, 'roles', model.roles);
			_ember['default'].set(controller, 'goals', model.goals);
		},
		actions: {
			newRole: function newRole() {
				var role = this.store.createRecord('role', {
					name: 'New role'
				});
				role.save();
			}
		}
	});
});
define('pa/routes/mission-statement', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({
		model: function model() {
			return this.store.findAll('mission-statement').then(function (statements) {
				return statements.get('firstObject');
			});
		}
	});
});
define('pa/routes/weekly-goals', ['exports', 'ember'], function (exports, _ember) {
	exports['default'] = _ember['default'].Route.extend({
		model: function model() {
			return _ember['default'].RSVP.hash({
				roles: this.store.findAll('role'),
				goals: this.store.findAll('weekly-goal')
			});
		},
		setupController: function setupController(controller, model) {
			this._super.apply(this, arguments);
			_ember['default'].set(controller, 'roles', model.roles);
			_ember['default'].set(controller, 'goals', model.goals);
		}
	});
});
define('pa/serializers/application', ['exports', 'ember-data'], function (exports, _emberData) {
  exports['default'] = _emberData['default'].JSONSerializer.extend({});
});
define("pa/templates/application", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 3,
              "column": 2
            },
            "end": {
              "line": 3,
              "column": 92
            }
          },
          "moduleName": "pa/templates/application.hbs"
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Mission statement");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child1 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 4,
              "column": 2
            },
            "end": {
              "line": 4,
              "column": 86
            }
          },
          "moduleName": "pa/templates/application.hbs"
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Longterm goals");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    var child2 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 5,
              "column": 2
            },
            "end": {
              "line": 5,
              "column": 82
            }
          },
          "moduleName": "pa/templates/application.hbs"
        },
        arity: 0,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("Weekly goals");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes() {
          return [];
        },
        statements: [],
        locals: [],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 11,
            "column": 0
          }
        },
        "moduleName": "pa/templates/application.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("section");
        dom.setAttribute(el1, "id", "menu-bar");
        var el2 = dom.createTextNode("\n	");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("div");
        dom.setAttribute(el2, "class", "btn-group-vertical");
        dom.setAttribute(el2, "role", "group");
        var el3 = dom.createTextNode("\n		");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n		");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n		");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("section");
        dom.setAttribute(el1, "id", "content");
        dom.setAttribute(el1, "class", "well");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0, 1]);
        var morphs = new Array(4);
        morphs[0] = dom.createMorphAt(element0, 1, 1);
        morphs[1] = dom.createMorphAt(element0, 3, 3);
        morphs[2] = dom.createMorphAt(element0, 5, 5);
        morphs[3] = dom.createMorphAt(dom.childAt(fragment, [2]), 1, 1);
        return morphs;
      },
      statements: [["block", "link-to", ["mission-statement"], ["tagName", "button", "class", "btn btn-primary"], 0, null, ["loc", [null, [3, 2], [3, 104]]]], ["block", "link-to", ["longterm-goals"], ["tagName", "button", "class", "btn btn-primary"], 1, null, ["loc", [null, [4, 2], [4, 98]]]], ["block", "link-to", ["weekly-goals"], ["tagName", "button", "class", "btn btn-primary"], 2, null, ["loc", [null, [5, 2], [5, 94]]]], ["content", "outlet", ["loc", [null, [9, 0], [9, 10]]]]],
      locals: [],
      templates: [child0, child1, child2]
    };
  })());
});
define("pa/templates/components/longterm-goal", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      var child0 = (function () {
        return {
          meta: {
            "revision": "Ember@1.13.11",
            "loc": {
              "source": null,
              "start": {
                "line": 72,
                "column": 3
              },
              "end": {
                "line": 77,
                "column": 3
              }
            },
            "moduleName": "pa/templates/components/longterm-goal.hbs"
          },
          arity: 0,
          cachedFragment: null,
          hasRendered: false,
          buildFragment: function buildFragment(dom) {
            var el0 = dom.createDocumentFragment();
            var el1 = dom.createTextNode("			");
            dom.appendChild(el0, el1);
            var el1 = dom.createElement("div");
            dom.setAttribute(el1, "class", "goal-row");
            var el2 = dom.createTextNode("\n				");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("a");
            dom.setAttribute(el2, "class", "goal-editable editable");
            var el3 = dom.createComment("");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n				");
            dom.appendChild(el1, el2);
            var el2 = dom.createElement("button");
            dom.setAttribute(el2, "type", "button");
            dom.setAttribute(el2, "class", "btn btn-success btn-xs pull-right");
            var el3 = dom.createElement("span");
            dom.setAttribute(el3, "class", "glyphicon glyphicon-trash");
            dom.appendChild(el2, el3);
            dom.appendChild(el1, el2);
            var el2 = dom.createTextNode("\n			");
            dom.appendChild(el1, el2);
            dom.appendChild(el0, el1);
            var el1 = dom.createTextNode("\n");
            dom.appendChild(el0, el1);
            return el0;
          },
          buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
            var element0 = dom.childAt(fragment, [1]);
            var element1 = dom.childAt(element0, [1]);
            var element2 = dom.childAt(element0, [3]);
            var morphs = new Array(3);
            morphs[0] = dom.createAttrMorph(element1, 'data-id');
            morphs[1] = dom.createMorphAt(element1, 0, 0);
            morphs[2] = dom.createElementMorph(element2);
            return morphs;
          },
          statements: [["attribute", "data-id", ["get", "goal.id", ["loc", [null, [74, 48], [74, 55]]]]], ["content", "goal.content", ["loc", [null, [74, 58], [74, 74]]]], ["element", "action", ["deleteGoal", ["get", "goal.id", ["loc", [null, [75, 90], [75, 97]]]]], [], ["loc", [null, [75, 68], [75, 99]]]]],
          locals: [],
          templates: []
        };
      })();
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 71,
              "column": 3
            },
            "end": {
              "line": 78,
              "column": 3
            }
          },
          "moduleName": "pa/templates/components/longterm-goal.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
          dom.insertBoundary(fragment, 0);
          dom.insertBoundary(fragment, null);
          return morphs;
        },
        statements: [["block", "unless", [["get", "goal.isDeleted", ["loc", [null, [72, 13], [72, 27]]]]], [], 0, null, ["loc", [null, [72, 3], [77, 14]]]]],
        locals: ["goal"],
        templates: [child0]
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 83,
            "column": 0
          }
        },
        "moduleName": "pa/templates/components/longterm-goal.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("style");
        var el2 = dom.createTextNode("\n.role-goal .table\n{\n	margin-bottom:0px;\n}\n.role-goal td\n{\n	vertical-align:middle !important;\n	padding:1px !important;\n}\n.role-goal .goal-row a\n{\n	margin-left: 10px;\n}\n.role-goal:nth-child(odd) .goal-row\n{\n	background-color: #D8EFF0;;\n}\n.role-goal:nth-child(even) .goal-row\n{\n	background-color: #F0F0D8;\n}\n.role-goal .btn-xs\n{\n	padding: 0px 5px;\n}\n.role-goal .role-btn-group\n{\n	position: absolute;\n	top: 0px;\n	right: 0px;\n	display:none;\n}\n.role-goal .role-cell:hover .role-btn-group\n{\n	display:table-cell;\n}\n.role-goal .goal-row button\n{\n	display:none;\n}\n.role-goal .goal-row:hover button\n{\n	display:inline-block;\n}\n.role-goal .role-cell\n{\n	position:relative;\n	text-align: center;\n	width:150px;\n	background-clip: padding-box;\n}\n.role-goal .editable\n{\n	min-width:calc(100% - 40px); \n	display: inline-block;\n	border-bottom:none !important;\n}\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("table");
        dom.setAttribute(el1, "class", "table table-bordered table-hover");
        var el2 = dom.createTextNode("\n	");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("tbody");
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("tr");
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        dom.setAttribute(el4, "class", "success role-cell");
        var el5 = dom.createTextNode("\n			");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("a");
        dom.setAttribute(el5, "class", "role-editable editable");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n			");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("span");
        dom.setAttribute(el5, "class", "btn-group pull-right role-btn-group");
        var el6 = dom.createTextNode("\n				");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("button");
        dom.setAttribute(el6, "type", "button");
        dom.setAttribute(el6, "class", "btn btn-success btn-xs");
        var el7 = dom.createElement("span");
        dom.setAttribute(el7, "class", "glyphicon glyphicon-trash");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n				");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("button");
        dom.setAttribute(el6, "type", "button");
        dom.setAttribute(el6, "class", "btn btn-success btn-xs");
        var el7 = dom.createElement("span");
        dom.setAttribute(el7, "class", "glyphicon glyphicon-plus");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n			");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n		");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("\n");
        dom.appendChild(el4, el5);
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("		");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n	");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element3 = dom.childAt(fragment, [2, 1, 1]);
        var element4 = dom.childAt(element3, [1]);
        var element5 = dom.childAt(element4, [3]);
        var element6 = dom.childAt(element5, [1]);
        var element7 = dom.childAt(element5, [3]);
        var morphs = new Array(4);
        morphs[0] = dom.createMorphAt(dom.childAt(element4, [1]), 0, 0);
        morphs[1] = dom.createElementMorph(element6);
        morphs[2] = dom.createElementMorph(element7);
        morphs[3] = dom.createMorphAt(dom.childAt(element3, [3]), 1, 1);
        return morphs;
      },
      statements: [["content", "role.name", ["loc", [null, [64, 37], [64, 50]]]], ["element", "action", ["deleteRole"], [], ["loc", [null, [66, 57], [66, 80]]]], ["element", "action", ["newGoal"], [], ["loc", [null, [67, 57], [67, 77]]]], ["block", "each", [["get", "goals", ["loc", [null, [71, 11], [71, 16]]]]], [], 0, null, ["loc", [null, [71, 3], [78, 12]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("pa/templates/components/table-cell", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 20,
              "column": 0
            },
            "end": {
              "line": 22,
              "column": 0
            }
          },
          "moduleName": "pa/templates/components/table-cell.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("a");
          dom.setAttribute(el1, "draggable", "true");
          dom.setAttribute(el1, "class", "event btn btn-info");
          dom.setAttribute(el1, "data-container", "body");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [0]);
          var morphs = new Array(3);
          morphs[0] = dom.createAttrMorph(element0, 'data-id');
          morphs[1] = dom.createAttrMorph(element0, 'title');
          morphs[2] = dom.createMorphAt(element0, 0, 0);
          return morphs;
        },
        statements: [["attribute", "data-id", ["get", "goal.id", ["loc", [null, [21, 30], [21, 37]]]]], ["attribute", "title", ["get", "goal.content", ["loc", [null, [21, 97], [21, 109]]]]], ["content", "goal.content", ["loc", [null, [21, 113], [21, 129]]]]],
        locals: ["goal"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 24,
            "column": 0
          }
        },
        "moduleName": "pa/templates/components/table-cell.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("style");
        var el2 = dom.createTextNode("\n	.table-cell\n	{\n		height:100%;\n	}\n	.table-cell .event\n	{\n		padding:0px 5px;\n		line-height: 18px;\n		height: 20px;\n		overflow:hidden;\n		font-size:12px;\n	}\n	.table-cell .event + .tooltip\n	{\n		position:absolute;\n	}\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "btn-group btn-group-justified");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [2]), 1, 1);
        return morphs;
      },
      statements: [["block", "each", [["get", "myGoals", ["loc", [null, [20, 8], [20, 15]]]]], [], 0, null, ["loc", [null, [20, 0], [22, 9]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("pa/templates/components/table-row", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 3,
              "column": 0
            }
          },
          "moduleName": "pa/templates/components/table-row.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createElement("td");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [0]), 0, 0);
          return morphs;
        },
        statements: [["inline", "table-cell", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [2, 23], [2, 28]]]]], [], []], "rowId", ["subexpr", "@mut", [["get", "id", ["loc", [null, [2, 35], [2, 37]]]]], [], []], "colId", ["subexpr", "@mut", [["get", "item", ["loc", [null, [2, 44], [2, 48]]]]], [], []]], ["loc", [null, [2, 4], [2, 50]]]]],
        locals: ["item"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 4,
            "column": 0
          }
        },
        "moduleName": "pa/templates/components/table-row.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        dom.insertBoundary(fragment, 0);
        dom.insertBoundary(fragment, null);
        return morphs;
      },
      statements: [["block", "each", [["get", "idList", ["loc", [null, [1, 8], [1, 14]]]]], [], 0, null, ["loc", [null, [1, 0], [3, 9]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("pa/templates/components/tinymce-editor", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 2,
            "column": 0
          }
        },
        "moduleName": "pa/templates/components/tinymce-editor.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "id", "statement");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [0]);
        var morphs = new Array(1);
        morphs[0] = dom.createElementMorph(element0);
        return morphs;
      },
      statements: [["element", "action", ["editStatement"], ["on", "doubleClick"], ["loc", [null, [1, 20], [1, 63]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("pa/templates/components/weekly-calendar", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 59,
              "column": 4
            },
            "end": {
              "line": 61,
              "column": 4
            }
          },
          "moduleName": "pa/templates/components/weekly-calendar.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("					");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("span");
          var el2 = dom.createComment("");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(dom.childAt(fragment, [1]), 0, 0);
          return morphs;
        },
        statements: [["content", "time", ["loc", [null, [60, 11], [60, 19]]]]],
        locals: ["time"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 87,
            "column": 0
          }
        },
        "moduleName": "pa/templates/components/weekly-calendar.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("style");
        var el2 = dom.createTextNode("\n	.weekly-calendar\n	{\n		width:77%;\n		float:right;\n	}\n	.weekly-calendar table\n	{\n		table-layout:fixed;\n	}\n	.weekly-calendar td\n	{\n		text-align:center;\n		height:20px;\n		padding:0px !important;\n	}\n	#time-label\n	{\n		position:absolute;\n		left:0px;\n		top:20px;\n		opacity:0.7;\n		z-index:1;\n	}\n	#time-label span\n	{\n		display:block;\n		font-size:10px;\n		line-height:20px;\n		-webkit-touch-callout: none;\n	    -webkit-user-select: none;\n	    -khtml-user-select: none;\n	    -moz-user-select: none;\n	    -ms-user-select: none;\n	    user-select: none;\n	}\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("table");
        dom.setAttribute(el1, "class", "table table-condensed table-bordered table-hover");
        var el2 = dom.createTextNode("\n	");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("tbody");
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("tr");
        dom.setAttribute(el3, "class", "info");
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("Sun");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("Mon");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("Tue");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("Wed");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("Thu");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("Fri");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("Sat");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n	");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("tr");
        dom.setAttribute(el3, "class", "success");
        var el4 = dom.createElement("td");
        dom.setAttribute(el4, "colspan", "7");
        var el5 = dom.createTextNode("Today's Priorities");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("tr");
        dom.setAttribute(el3, "class", "success");
        dom.setAttribute(el3, "style", "position:relative;");
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        dom.setAttribute(el4, "colspan", "7");
        var el5 = dom.createTextNode("Apointments/Commitments\n			");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("span");
        dom.setAttribute(el5, "id", "time-label");
        var el6 = dom.createTextNode("\n");
        dom.appendChild(el5, el6);
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("			");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n		");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n	");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("tr");
        dom.setAttribute(el3, "class", "success");
        var el4 = dom.createElement("td");
        dom.setAttribute(el4, "colspan", "7");
        var el5 = dom.createTextNode("Evening");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createComment("");
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [2, 1]);
        var morphs = new Array(26);
        morphs[0] = dom.createMorphAt(element0, 5, 5);
        morphs[1] = dom.createMorphAt(element0, 7, 7);
        morphs[2] = dom.createMorphAt(element0, 9, 9);
        morphs[3] = dom.createMorphAt(element0, 11, 11);
        morphs[4] = dom.createMorphAt(element0, 13, 13);
        morphs[5] = dom.createMorphAt(element0, 15, 15);
        morphs[6] = dom.createMorphAt(dom.childAt(element0, [17, 1, 1]), 1, 1);
        morphs[7] = dom.createMorphAt(element0, 19, 19);
        morphs[8] = dom.createMorphAt(element0, 21, 21);
        morphs[9] = dom.createMorphAt(element0, 23, 23);
        morphs[10] = dom.createMorphAt(element0, 25, 25);
        morphs[11] = dom.createMorphAt(element0, 27, 27);
        morphs[12] = dom.createMorphAt(element0, 29, 29);
        morphs[13] = dom.createMorphAt(element0, 31, 31);
        morphs[14] = dom.createMorphAt(element0, 33, 33);
        morphs[15] = dom.createMorphAt(element0, 35, 35);
        morphs[16] = dom.createMorphAt(element0, 37, 37);
        morphs[17] = dom.createMorphAt(element0, 39, 39);
        morphs[18] = dom.createMorphAt(element0, 41, 41);
        morphs[19] = dom.createMorphAt(element0, 43, 43);
        morphs[20] = dom.createMorphAt(element0, 47, 47);
        morphs[21] = dom.createMorphAt(element0, 49, 49);
        morphs[22] = dom.createMorphAt(element0, 51, 51);
        morphs[23] = dom.createMorphAt(element0, 53, 53);
        morphs[24] = dom.createMorphAt(element0, 55, 55);
        morphs[25] = dom.createMorphAt(element0, 57, 57);
        return morphs;
      },
      statements: [["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [50, 19], [50, 24]]]]], [], []], "id", 0], ["loc", [null, [50, 1], [50, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [51, 19], [51, 24]]]]], [], []], "id", 1], ["loc", [null, [51, 1], [51, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [52, 19], [52, 24]]]]], [], []], "id", 2], ["loc", [null, [52, 1], [52, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [53, 19], [53, 24]]]]], [], []], "id", 3], ["loc", [null, [53, 1], [53, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [54, 19], [54, 24]]]]], [], []], "id", 4], ["loc", [null, [54, 1], [54, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [55, 19], [55, 24]]]]], [], []], "id", 5], ["loc", [null, [55, 1], [55, 31]]]], ["block", "each", [["get", "times", ["loc", [null, [59, 12], [59, 17]]]]], [], 0, null, ["loc", [null, [59, 4], [61, 13]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [65, 19], [65, 24]]]]], [], []], "id", 6], ["loc", [null, [65, 1], [65, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [66, 19], [66, 24]]]]], [], []], "id", 7], ["loc", [null, [66, 1], [66, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [67, 19], [67, 24]]]]], [], []], "id", 8], ["loc", [null, [67, 1], [67, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [68, 19], [68, 24]]]]], [], []], "id", 9], ["loc", [null, [68, 1], [68, 31]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [69, 19], [69, 24]]]]], [], []], "id", 10], ["loc", [null, [69, 1], [69, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [70, 19], [70, 24]]]]], [], []], "id", 11], ["loc", [null, [70, 1], [70, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [71, 19], [71, 24]]]]], [], []], "id", 12], ["loc", [null, [71, 1], [71, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [72, 19], [72, 24]]]]], [], []], "id", 13], ["loc", [null, [72, 1], [72, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [73, 19], [73, 24]]]]], [], []], "id", 14], ["loc", [null, [73, 1], [73, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [74, 19], [74, 24]]]]], [], []], "id", 15], ["loc", [null, [74, 1], [74, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [75, 19], [75, 24]]]]], [], []], "id", 16], ["loc", [null, [75, 1], [75, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [76, 19], [76, 24]]]]], [], []], "id", 17], ["loc", [null, [76, 1], [76, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [77, 19], [77, 24]]]]], [], []], "id", 18], ["loc", [null, [77, 1], [77, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [79, 19], [79, 24]]]]], [], []], "id", 19], ["loc", [null, [79, 1], [79, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [80, 19], [80, 24]]]]], [], []], "id", 20], ["loc", [null, [80, 1], [80, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [81, 19], [81, 24]]]]], [], []], "id", 21], ["loc", [null, [81, 1], [81, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [82, 19], [82, 24]]]]], [], []], "id", 22], ["loc", [null, [82, 1], [82, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [83, 19], [83, 24]]]]], [], []], "id", 23], ["loc", [null, [83, 1], [83, 32]]]], ["inline", "table-row", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [84, 19], [84, 24]]]]], [], []], "id", 24], ["loc", [null, [84, 1], [84, 32]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("pa/templates/components/weekly-goal", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 96,
              "column": 3
            },
            "end": {
              "line": 102,
              "column": 3
            }
          },
          "moduleName": "pa/templates/components/weekly-goal.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("				");
          dom.appendChild(el0, el1);
          var el1 = dom.createElement("div");
          dom.setAttribute(el1, "class", "goal-row");
          dom.setAttribute(el1, "draggable", "true");
          var el2 = dom.createTextNode("\n					");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("a");
          dom.setAttribute(el2, "class", "priority-editable priority");
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "label label-default");
          var el4 = dom.createComment("");
          dom.appendChild(el3, el4);
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n					");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("a");
          dom.setAttribute(el2, "class", "goal-editable editable");
          var el3 = dom.createComment("");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n					");
          dom.appendChild(el1, el2);
          var el2 = dom.createElement("button");
          dom.setAttribute(el2, "type", "button");
          dom.setAttribute(el2, "class", "btn btn-success btn-xs pull-right goal-button");
          var el3 = dom.createElement("span");
          dom.setAttribute(el3, "class", "glyphicon glyphicon-trash");
          dom.appendChild(el2, el3);
          dom.appendChild(el1, el2);
          var el2 = dom.createTextNode("\n				");
          dom.appendChild(el1, el2);
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var element0 = dom.childAt(fragment, [1]);
          var element1 = dom.childAt(element0, [1]);
          var element2 = dom.childAt(element0, [3]);
          var element3 = dom.childAt(element0, [5]);
          var morphs = new Array(7);
          morphs[0] = dom.createAttrMorph(element0, 'data-id');
          morphs[1] = dom.createAttrMorph(element1, 'data-id');
          morphs[2] = dom.createAttrMorph(element1, 'data-value');
          morphs[3] = dom.createMorphAt(dom.childAt(element1, [0]), 0, 0);
          morphs[4] = dom.createAttrMorph(element2, 'data-id');
          morphs[5] = dom.createMorphAt(element2, 0, 0);
          morphs[6] = dom.createElementMorph(element3);
          return morphs;
        },
        statements: [["attribute", "data-id", ["get", "goal.id", ["loc", [null, [97, 53], [97, 60]]]]], ["attribute", "data-id", ["get", "goal.id", ["loc", [null, [98, 53], [98, 60]]]]], ["attribute", "data-value", ["get", "goal.priority", ["loc", [null, [98, 76], [98, 89]]]]], ["content", "goal.priority", ["loc", [null, [98, 126], [98, 143]]]], ["attribute", "data-id", ["get", "goal.id", ["loc", [null, [99, 49], [99, 56]]]]], ["content", "goal.content", ["loc", [null, [99, 59], [99, 75]]]], ["element", "action", ["deleteGoal", ["get", "goal.id", ["loc", [null, [100, 103], [100, 110]]]]], [], ["loc", [null, [100, 81], [100, 112]]]]],
        locals: ["goal"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 107,
            "column": 0
          }
        },
        "moduleName": "pa/templates/components/weekly-goal.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("style");
        var el2 = dom.createTextNode("\n.weekly-goal .table-role\n{\n	margin-bottom:0px;\n	width:100%;\n	table-layout:fixed;\n}\n.weekly-goal .goal-row\n{\n	position:relative;\n	margin:1px 1px;\n	border-color: #DDD;\n    border-width: 1px;\n    border-style: solid;\n}\n.weekly-goal td\n{\n	vertical-align:middle !important;\n	padding:1px !important;\n}\n.weekly-goal .goal-row a\n{\n	margin-left: 10px;\n	word-break:break-all;\n	font-size:12px;\n}\n.weekly-goal .goal-row:hover .priority\n{\n	right:24px;\n}\n.weekly-goal .goal-row .priority\n{\n	position:absolute;\n	right:0px;\n	bottom:2px;\n}\n.weekly-goal:nth-child(odd) .goal-row\n{\n	background-color: #D8EFF0;;\n}\n.weekly-goal:nth-child(even) .goal-row\n{\n	background-color: #F0F0D8;\n}\n.weekly-goal .btn-xs\n{\n	padding: 0px 5px;\n}\n.weekly-goal .role-btn-group\n{\n	position: absolute;\n	top: 0px;\n	right: 0px;\n	display:none;\n}\n.weekly-goal .role-cell:hover .role-btn-group\n{\n	display:table-cell;\n}\n.weekly-goal .goal-row .goal-button\n{\n	position:absolute;\n	right:0px;\n}\n.weekly-goal .goal-row button\n{\n	display:none;\n}\n.weekly-goal .goal-row:hover button\n{\n	display:inline-block;\n}\n.weekly-goal .role-cell\n{\n	position:relative;\n	text-align: center;\n	background-clip: padding-box;\n	width:50%;\n}\n.weekly-goal .editable\n{\n	display: inline-block;\n	border-bottom:none !important;\n}\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("table");
        dom.setAttribute(el1, "class", "table table-bordered table-hover table-role");
        var el2 = dom.createTextNode("\n	");
        dom.appendChild(el1, el2);
        var el2 = dom.createElement("tbody");
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        var el3 = dom.createElement("tr");
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        dom.setAttribute(el4, "class", "success role-cell");
        var el5 = dom.createTextNode("\n			");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("a");
        dom.setAttribute(el5, "class", "role-editable editable");
        var el6 = dom.createComment("");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n			");
        dom.appendChild(el4, el5);
        var el5 = dom.createElement("span");
        dom.setAttribute(el5, "class", "btn-group pull-right role-btn-group");
        var el6 = dom.createTextNode("\n				");
        dom.appendChild(el5, el6);
        var el6 = dom.createElement("button");
        dom.setAttribute(el6, "type", "button");
        dom.setAttribute(el6, "class", "btn btn-success btn-xs");
        var el7 = dom.createElement("span");
        dom.setAttribute(el7, "class", "glyphicon glyphicon-plus");
        dom.appendChild(el6, el7);
        dom.appendChild(el5, el6);
        var el6 = dom.createTextNode("\n			");
        dom.appendChild(el5, el6);
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("\n		");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n		");
        dom.appendChild(el3, el4);
        var el4 = dom.createElement("td");
        var el5 = dom.createTextNode("\n");
        dom.appendChild(el4, el5);
        var el5 = dom.createComment("");
        dom.appendChild(el4, el5);
        var el5 = dom.createTextNode("		");
        dom.appendChild(el4, el5);
        dom.appendChild(el3, el4);
        var el4 = dom.createTextNode("\n	");
        dom.appendChild(el3, el4);
        dom.appendChild(el2, el3);
        var el3 = dom.createTextNode("\n	");
        dom.appendChild(el2, el3);
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element4 = dom.childAt(fragment, [2, 1, 1]);
        var element5 = dom.childAt(element4, [1]);
        var element6 = dom.childAt(element5, [3, 1]);
        var morphs = new Array(3);
        morphs[0] = dom.createMorphAt(dom.childAt(element5, [1]), 0, 0);
        morphs[1] = dom.createElementMorph(element6);
        morphs[2] = dom.createMorphAt(dom.childAt(element4, [3]), 1, 1);
        return morphs;
      },
      statements: [["content", "role.name", ["loc", [null, [90, 37], [90, 50]]]], ["element", "action", ["newGoal"], [], ["loc", [null, [92, 57], [92, 77]]]], ["block", "each", [["get", "goals", ["loc", [null, [96, 11], [96, 16]]]]], [], 0, null, ["loc", [null, [96, 3], [102, 12]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("pa/templates/index", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 1,
            "column": 0
          }
        },
        "moduleName": "pa/templates/index.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        return el0;
      },
      buildRenderNodes: function buildRenderNodes() {
        return [];
      },
      statements: [],
      locals: [],
      templates: []
    };
  })());
});
define("pa/templates/longterm-goals", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 1,
              "column": 0
            },
            "end": {
              "line": 3,
              "column": 0
            }
          },
          "moduleName": "pa/templates/longterm-goals.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("		");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [["inline", "longterm-goal", [], ["role", ["subexpr", "@mut", [["get", "role", ["loc", [null, [2, 23], [2, 27]]]]], [], []], "all-goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [2, 38], [2, 43]]]]], [], []]], ["loc", [null, [2, 2], [2, 45]]]]],
        locals: ["role"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 5,
            "column": 0
          }
        },
        "moduleName": "pa/templates/longterm-goals.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("button");
        dom.setAttribute(el1, "class", "btn btn-primary");
        var el2 = dom.createElement("span");
        dom.setAttribute(el2, "class", "glyphicon glyphicon-plus");
        dom.appendChild(el1, el2);
        var el2 = dom.createTextNode("New Role");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var element0 = dom.childAt(fragment, [1]);
        var morphs = new Array(2);
        morphs[0] = dom.createMorphAt(fragment, 0, 0, contextualElement);
        morphs[1] = dom.createElementMorph(element0);
        dom.insertBoundary(fragment, 0);
        return morphs;
      },
      statements: [["block", "each", [["get", "roles", ["loc", [null, [1, 8], [1, 13]]]]], [], 0, null, ["loc", [null, [1, 0], [3, 9]]]], ["element", "action", ["newRole"], [], ["loc", [null, [4, 8], [4, 28]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
define("pa/templates/mission-statement", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 31,
            "column": 0
          }
        },
        "moduleName": "pa/templates/mission-statement.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("style");
        var el2 = dom.createTextNode("\n	#statement\n	{\n		height:100%;\n		outline-color: #2F2F2F;\n	}\n	.mce-edit-focus\n	{\n		outline-style:inset;\n	}\n	.editer-view\n	{\n		height:100%;\n	}\n	.toolbar-bottom .mce-floatpanel\n	{\n		bottom: 0px;\n		position: absolute;\n		top: auto !important;\n		height:auto;\n	}\n	#toolbar\n	{\n		position:fixed;\n		min-width:1px;\n		min-height:0px;\n	}\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "id", "toolbar");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(1);
        morphs[0] = dom.createMorphAt(fragment, 2, 2, contextualElement);
        return morphs;
      },
      statements: [["inline", "tinymce-editor", [], ["value", ["subexpr", "@mut", [["get", "model.statement", ["loc", [null, [29, 23], [29, 38]]]]], [], []], "onBlur", ["subexpr", "action", ["tinymceChanged"], [], ["loc", [null, [29, 46], [29, 71]]]]], ["loc", [null, [29, 0], [29, 73]]]]],
      locals: [],
      templates: []
    };
  })());
});
define("pa/templates/weekly-goals", ["exports"], function (exports) {
  exports["default"] = Ember.HTMLBars.template((function () {
    var child0 = (function () {
      return {
        meta: {
          "revision": "Ember@1.13.11",
          "loc": {
            "source": null,
            "start": {
              "line": 11,
              "column": 0
            },
            "end": {
              "line": 13,
              "column": 0
            }
          },
          "moduleName": "pa/templates/weekly-goals.hbs"
        },
        arity: 1,
        cachedFragment: null,
        hasRendered: false,
        buildFragment: function buildFragment(dom) {
          var el0 = dom.createDocumentFragment();
          var el1 = dom.createTextNode("		");
          dom.appendChild(el0, el1);
          var el1 = dom.createComment("");
          dom.appendChild(el0, el1);
          var el1 = dom.createTextNode("\n");
          dom.appendChild(el0, el1);
          return el0;
        },
        buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
          var morphs = new Array(1);
          morphs[0] = dom.createMorphAt(fragment, 1, 1, contextualElement);
          return morphs;
        },
        statements: [["inline", "weekly-goal", [], ["role", ["subexpr", "@mut", [["get", "role", ["loc", [null, [12, 21], [12, 25]]]]], [], []], "all-goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [12, 36], [12, 41]]]]], [], []]], ["loc", [null, [12, 2], [12, 43]]]]],
        locals: ["role"],
        templates: []
      };
    })();
    return {
      meta: {
        "revision": "Ember@1.13.11",
        "loc": {
          "source": null,
          "start": {
            "line": 1,
            "column": 0
          },
          "end": {
            "line": 16,
            "column": 0
          }
        },
        "moduleName": "pa/templates/weekly-goals.hbs"
      },
      arity: 0,
      cachedFragment: null,
      hasRendered: false,
      buildFragment: function buildFragment(dom) {
        var el0 = dom.createDocumentFragment();
        var el1 = dom.createElement("style");
        var el2 = dom.createTextNode("\n	.weekly-goals\n	{\n		width:23%;\n		float:left;\n		z-index:2;\n		position:relative;\n	}\n");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createElement("div");
        dom.setAttribute(el1, "class", "weekly-goals");
        var el2 = dom.createTextNode("\n");
        dom.appendChild(el1, el2);
        var el2 = dom.createComment("");
        dom.appendChild(el1, el2);
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        var el1 = dom.createComment("");
        dom.appendChild(el0, el1);
        var el1 = dom.createTextNode("\n");
        dom.appendChild(el0, el1);
        return el0;
      },
      buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
        var morphs = new Array(2);
        morphs[0] = dom.createMorphAt(dom.childAt(fragment, [2]), 1, 1);
        morphs[1] = dom.createMorphAt(fragment, 4, 4, contextualElement);
        return morphs;
      },
      statements: [["block", "each", [["get", "roles", ["loc", [null, [11, 8], [11, 13]]]]], [], 0, null, ["loc", [null, [11, 0], [13, 9]]]], ["inline", "weekly-calendar", [], ["goals", ["subexpr", "@mut", [["get", "goals", ["loc", [null, [15, 24], [15, 29]]]]], [], []]], ["loc", [null, [15, 0], [15, 31]]]]],
      locals: [],
      templates: [child0]
    };
  })());
});
/* jshint ignore:start */

/* jshint ignore:end */

/* jshint ignore:start */

define('pa/config/environment', ['ember'], function(Ember) {
  var prefix = 'pa';
/* jshint ignore:start */

try {
  var metaName = prefix + '/config/environment';
  var rawConfig = Ember['default'].$('meta[name="' + metaName + '"]').attr('content');
  var config = JSON.parse(unescape(rawConfig));

  return { 'default': config };
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

/* jshint ignore:end */

});

if (!runningTests) {
  require("pa/app")["default"].create({"name":"pa","version":"0.0.0+9428c5f3"});
}

/* jshint ignore:end */
//# sourceMappingURL=pa.map