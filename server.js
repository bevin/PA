var deployd = require('deployd');

var server = deployd({
  port: process.env.OPENSHIFT_NODEJS_PORT|8080 ,
  host: process.env.OPENSHIFT_NODEJS_IP,
  env: 'production',
  db: {
    host: process.env.OPENSHIFT_MONGODB_DB_HOST,
    port: process.env.OPENSHIFT_MONGODB_DB_PORT,
    name: 'pim',
    credentials: {
      username: 'admin',
      password: 'qDYQKLRg7hNv'
    }
  }
});

server.listen();

server.on('listening', function() {
  console.log("Server is listening");
});

server.on('error', function(err) {
  console.error(err);
  process.nextTick(function() { // Give the server a chance to return an error
    process.exit();
  });
});
